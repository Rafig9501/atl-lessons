import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;

public class Main {

    // ArrayList meseleleri:

    // 1. Verilen arraylisti iterate edib her bir elementini console-a verin
    // 2. Verilen arraylistin 4-cu indexdeki elementini yerini 1-ci ile deyishin
    // 3. Verilen arraylist-in elementlerinin yerini random shekilde deyishin
    // 4. Verilen arraylist-in initial capacity-sini azaldin

    // LinkedList meseleleri:

    // 1. Verilen linkedlist-in ilk elementinin qabagina bashqa bir element qoyun: meselen:
    // [Red, Green, Black] -> [Pink, Red, Green, Black]
    // 2. Verilen linkedlist-in sonuncu elementinden sonra bir element qoyun: meselen:
    // [Red, Green, Red, Green] -> [Red, Green, Red, Green, Pink]
    // 3. Verilen linkedlist-in birinci element-ini consola print edin
    // 4. Verilen linkedlist-in sonuncu element-ini consola print edib remove edin

    public static void main(String[] args) {

        ArrayList<String> strings = new ArrayList<>();
        strings.add("23");
        strings.add("24");
        strings.add("25");
        strings.add("26");
        strings.add("27");
        strings.add("28");
        strings.add("29");
        strings.add("30");

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("Red");
        linkedList.add("Green");
        linkedList.add("Black");
        linkedList.add("White");

        removeLastElementAndPrint(linkedList);
    }

    private static void iterateAndPrintArraylist(ArrayList<String> strings) {
        strings.forEach(System.out::println);
    }

    private static void swapElementsInArraylist(ArrayList<String> strings, int indexOne, int indexTwo) {

        strings.forEach(s -> System.out.print(s + " "));
        // 1. Solution - 1
        String s1 = strings.get(indexOne);
        String s2 = strings.get(indexTwo);
        strings.set(indexTwo, s1);
        strings.set(indexOne, s2);

        // 1. Solution - 2
        Collections.swap(strings, indexOne, indexTwo);

        System.out.println();
        strings.forEach(s -> System.out.print(s + " "));
    }

    private static void shuffleArraylist(ArrayList<String> strings) {
        strings.forEach(s -> System.out.print(s + " "));
        Collections.shuffle(strings);
        System.out.println();
        strings.forEach(s -> System.out.print(s + " "));
    }

    private static void decreaseInitialCapacityOfArraylist(ArrayList<String> strings) {
        strings.trimToSize();
    }

    private static void putAnElementOnFrontOfLinkedList(LinkedList<String> strings, String element) {
        strings.forEach(s -> System.out.print(s + " "));
        strings.offerFirst(element);
        System.out.println();
        strings.forEach(s -> System.out.print(s + " "));
    }

    private static void addElementAtTheEndOfLinkedList(LinkedList<String> strings, String element) {
        strings.forEach(s -> System.out.print(s + " "));
        strings.offerLast(element);
        System.out.println();
        strings.forEach(s -> System.out.print(s + " "));
    }

    private static void printFirstELementInLinkedList(LinkedList<String> strings){
        System.out.println(strings.peek());
    }

    private static void removeLastElementAndPrint(LinkedList<String> strings){
        strings.forEach(s -> System.out.print(s + " "));
        System.out.println();
        System.out.println(strings.removeLast());
        strings.forEach(s -> System.out.print(s + " "));
    }
}
