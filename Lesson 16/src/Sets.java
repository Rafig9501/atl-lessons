import java.util.*;

public class Sets {

    public static void main(String[] args) {
        TreeSet<String> ts = new TreeSet<String>();

        // Adding elements into the TreeSet
        // using add()
        ts.add("India");
        ts.add("Australia");
        ts.add("South Africa");

        Iterator<String> iterator = ts.descendingIterator();
        Queue<String> priority = new PriorityQueue<>();
        priority.add("21");
        priority.add("22");
        priority.add("23");
        priority.add("24");
        priority.add("25");
        priority.add("26");
        priority.add("27");
        priority.add("28");
        priority.add("29");
        priority.add("30");
        priority.add("31");
        priority.add("31");
        System.out.println(priority);
    }
}
