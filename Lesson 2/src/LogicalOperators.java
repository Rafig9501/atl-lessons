public class LogicalOperators {

    public static void main(String[] args) {

        int a = 36;
        int b = 10;
        int c = 11;

        boolean boolean1 = (a > b);
        boolean boolean2 = (a < c);
        boolean boolean3 = (b > c);

        System.out.println(boolean1 && boolean2 && boolean3);

        System.out.println((a < b) || (a < c));
    }
}
