public class IncrementOperators {

    public static void main(String[] args) {

        int a = 9;
        int b = 6;

        System.out.println(a++); // 9
        System.out.println(a); // 10

        System.out.println(++b);
        System.out.println(b);
    }
}
