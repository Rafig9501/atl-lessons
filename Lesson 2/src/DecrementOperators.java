public class DecrementOperators {

    public static void main(String[] args) {

        int a = 8;
        int b = 10;

        System.out.println(a--);
        System.out.println(a);
        System.out.println(--b);
        System.out.println(b);
    }
}
