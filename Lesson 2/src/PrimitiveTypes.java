public class PrimitiveTypes {

    public static void main(String[] args) {

        byte byte1 = 45;
        short short1 = 24213;
        int int1 = 432435;
        long long1 = 4325432584234L;
        float float1 = 23435.3234f;
        double double1 = 23435.3234;
        boolean boolean1 = true;
        char char1 = 'r';

        System.out.println(byte1);
        System.out.println(short1);
        System.out.println(int1);
        System.out.println(long1);
        System.out.println(float1);
        System.out.println(double1);
        System.out.println(boolean1);
        System.out.println(char1);
    }
}