package animal;

public class Lion extends Animal{

    public Lion(String name, int age) {
        super(name, age);
    }

    @Override
    public void printName() {
        System.out.println("Lion: name = " + this.getName());
    }
}