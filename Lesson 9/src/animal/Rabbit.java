package animal;

import rabbit.RabbitChild;

public class Rabbit extends Animal {

    protected String eyesColor;

    public Rabbit(String name, int age) {
        super(name, age);
    }

    public void printRabbitParentEyesColor(){
        RabbitChild rabbitChild = new RabbitChild("rabbit", 21);
        rabbitChild.eyesColor = "green";
    }
}
