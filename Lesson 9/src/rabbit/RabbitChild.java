package rabbit;

import animal.Rabbit;

public class RabbitChild extends Rabbit {

    public RabbitChild(String name, int age) {
        super(name, age);
    }
}
