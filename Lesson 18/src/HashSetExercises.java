import java.util.*;

public class HashSetExercises {

    // 1. Write a Java program to add element to the hash set
    // 2. Write a Java program to convert a hash set to a List/ArrayList
    // 3. Write a Java program to convert a hash set to an array.
    // 4. Write a Java program to convert a hash set to a tree set
    // 5. Write a Java program to compare two sets and retain elements which are same on both sets
    // 6. Write a Java program to remove all of the elements from a hash set
    // 7. Write a Java program to create a new tree set, add some colors (string) and print out the tree set
    // 8. Write a Java program to iterate through all elements in a tree set.
    // 9. Write a Java program to get the number of elements in a tree set
    // 10. Write a Java program to compare two tree sets
    // 11. Write a Java program to retrieve and remove the first element of a tree set
    // 12. Write a Java program to retrieve and remove the last element of a tree set

    public static void main(String[] args) {

        HashSet<String> hashSet1 = new HashSet<>();
        HashSet<String> hashSet2 = new HashSet<>();

        TreeSet<String> treeSet1 = new TreeSet<>();
        TreeSet<String> treeSet2 = new TreeSet<>();

        hashSet1.add("Red");
        hashSet2.add("Blue");
        hashSet1.add("Blue");
        hashSet1.add("Orange");
        hashSet2.add("Black");
        hashSet1.add("Black");
        hashSet1.add("Green");
        hashSet1.add("Purple");
        hashSet2.add("Purple");

        treeSet1.add("Red");
        treeSet2.add("Blue");
        treeSet1.add("Blue");
        treeSet1.add("Orange");
        treeSet2.add("Black");
        treeSet1.add("Black");
        treeSet1.add("Green");
        treeSet1.add("Purple");
        treeSet2.add("Purple");

        convertingArray(hashSet2);
    }

    private static void compareAndRetainHashSet(HashSet<String> hashSet1, HashSet<String> hashSet2) {
        hashSet1.retainAll(hashSet2);
        System.out.println(hashSet1);
    }

    private static void compareAndTreeSet(TreeSet<String> treeSet1, TreeSet<String> treeSet2) {
        treeSet1.retainAll(treeSet2);
        System.out.println(treeSet1);
    }

    private static String[] convertingArray(HashSet<String> strings) {
        String[] array = new String[strings.size()];
        return strings.toArray(array);
    }
}
