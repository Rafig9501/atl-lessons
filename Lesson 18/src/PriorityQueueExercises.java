import java.util.*;

public class PriorityQueueExercises {

    //1. Write a Java program to iterate through all elements in priority queue
    //2. Write a Java program to insert a given element into a priority queue
    //3. Write a Java program to remove all the elements from a priority queue
    //4. Write a Java program to count the number of elements in a priority queue
    //5. Write a Java program to retrieve the first element of the priority queue
    //6. Write a Java program to change priorityQueue to maximum priorityqueue

    public static void main(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.add(10);
        queue.add(22);
        queue.add(36);
        queue.add(25);
        queue.add(16);
        queue.add(70);
        queue.add(82);
        queue.add(89);
        queue.add(14);
        showPriorityQueue(queue);
    }

    private static void iterateThroughPriorityQueue(PriorityQueue<Integer> priorityQueue) {
        Iterator<Integer> iterator = priorityQueue.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    private static void insertElementToPriorityQueue(PriorityQueue<Integer> priorityQueue) {
        priorityQueue.offer(Integer.valueOf(17));
        System.out.println(priorityQueue);
    }

    private static void removeAllElementFromPriorityQueue(PriorityQueue<Integer> priorityQueue) {
        priorityQueue.clear();
        System.out.println(priorityQueue);
    }

    private static void countElementsInPriorityQueue(PriorityQueue<Integer> priorityQueue) {
        System.out.println(priorityQueue.size());
    }

    private static void retrieveFirstElementFromPriorityQueue(PriorityQueue<Integer> priorityQueue) {
        System.out.println(priorityQueue.peek());
    }

    private static void showPriorityQueue(PriorityQueue<Integer> priorityQueue) {
        Integer value;
        while ((value = priorityQueue.poll()) != null) {
            System.out.print(value + " \n");
        }
    }
}