import java.util.*;
import java.util.stream.*;

public class StreamsOperations {
    public static void main(String[] args) {
        IntStream intStream = IntStream.range(0, 100);
        LongStream longStream = LongStream.range(0, 100);
        DoubleStream doubleStream = DoubleStream.of(12.3, 123.1, 98.0);
        //        intStream.forEach(System.out::println);
        //        generatingRandomNumbers(10, 10, 100);
        int[] ints = new Random()
            .ints(10, 1, 100)
            .toArray();
        for (int i = 0; i < ints.length; i++) {
            System.out.print(ints[i] + " ");
        }
        System.out.println();
        Arrays.sort(ints);
        for (int i = 0; i < ints.length; i++) {
            System.out.print(ints[i] + " ");
        }
    }

    private static void print(String... strings) {
        Arrays.stream(strings).forEach(System.out::println);
    }

    private static void generatingRandomNumbers(Integer size,
        Integer min, Integer max) {
        new Random().ints(size, min, max).forEach(System.out::println);
    }
}
