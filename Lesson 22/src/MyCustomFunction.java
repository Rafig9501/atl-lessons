@FunctionalInterface
public interface MyCustomFunction<T, P> {

    P doSomething(T t);
}
