import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> numbersInString = new ArrayList<>();
        numbersInString.add("19");
        numbersInString.add("13");
        numbersInString.add("11");
        numbersInString.add("18");
        numbersInString.add("15");
        numbersInString.add("17");
        numbersInString.stream().map(t -> t);
        numbersInString.forEach(t -> System.out.println(t));
        numbersInString.stream().collect(Collectors.toList());
        numbersInString.stream()
            .anyMatch(t -> Integer.parseInt(t) == 17);
        Optional<String> first = numbersInString.stream().findFirst();
        long count = numbersInString.stream().count();
        ArrayList<Integer> integers = new ArrayList<>();

        MyCustomFunction<String, Integer> function = t -> t.length();
        MyCustomFunction<String, Integer> function2 =
            new MyCustomFunction<String, Integer>() {
                @Override
                public Integer doSomething(String s) {
                    return null;
                }
            };
        //        MyCustomFunction<String, Student> function2 = s -> new Student(s);

        Predicate<String> predicate = new Predicate<String>() {

            @Override
            public boolean test(String s) {
                return false;
            }
        };
        List<Integer> integers1 = numbersInString.stream()
            .map(t -> function.doSomething(t)).toList();
        for (String i : numbersInString) {
            integers.add(function.doSomething(i));
        }
    }
}