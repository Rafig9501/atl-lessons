import java.util.function.Predicate;

public class Student implements Predicate<String> {

    private final String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean test(String s) {
        return false;
    }
}
