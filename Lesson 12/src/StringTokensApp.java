import java.util.Scanner;

public class StringTokensApp {

    public static void main(String[] args) {

        //       He is a very very good boy, isn't he?

        Scanner sc = new Scanner(System.in);
        String S = sc.nextLine();
        String[] tokens = S.split("[^a-zA-Z]");
        int numTokens = 0;

        for (String s : tokens) {
            if (s.length() > 0) {
                numTokens++;
            }
        }

        System.out.println(numTokens);

        for (String token : tokens) {
            if (token.length() > 0) {
                System.out.println(token);
            }
        }
    }
}
