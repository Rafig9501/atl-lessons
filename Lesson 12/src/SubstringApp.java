import java.util.Scanner;

public class SubstringApp {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String s = in.next();
        int start = in.nextInt();
        int end = in.nextInt();

        // HelloWorld

        String substring = s.substring(start, end);
        System.out.println(substring);
    }
}