import java.util.*;
import java.util.stream.Collectors;

public class SortingArrayList {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(78);
        list.add(19);
        list.add(24);
        list.add(43);
        list.add(1);
        list.add(15);

        list = list.stream().sorted().collect(Collectors.toList());
        list.forEach(System.out::println);
    }
}
