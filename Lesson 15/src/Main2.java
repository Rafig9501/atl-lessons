import java.util.*;
import java.lang.reflect.Field;

public class Main2 {
    // ArrayList meseleleri:

    // 1. Verilen arraylisti iterate edib her bir elementini console-a verin
    // 2. Verilen arraylistin 4-cu indexdeki elementini yerini 1-ci ile deyishin
    // 3. Verilen arraylist-in elementlerinin yerini random shekilde deyishin
    // 4. Verilen arraylist-in initial capacity-sini azaldin

    // LinkedList meseleleri:

    // 1. Verilen linkedlist-in ilk elementinin qabagina bashqa bir element qoyun: meselen:
    // [Red, Green, Black] -> [Pink, Red, Green, Black]
    // 2. Verilen linkedlist-in sonuncu elementinden sonra bir element qoyun: meselen:
    // [Red, Green, Red, Green] -> [Red, Green, Red, Green, Pink]
    // 3. Verilen linkedlist-in birinci element-ini consola print edin
    // 4. Verilen linkedlist-in sonuncu element-ini consola print edib remove edin

    public static void main(String[] args) throws Exception {

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("Red");
        linkedList.add("Green");
        linkedList.add("Black");
        linkedList.add("White");

        ArrayList<String> strings = new ArrayList<>();
        strings.add("23");
        strings.add("24");
        strings.add("25");
        strings.add("26");
        strings.add("27");
        strings.add("28");
        strings.add("29");
        strings.add("30");

        PriorityQueue<Integer> pQueue = new PriorityQueue<>();

        System.out.println(getCapacity(linkedList));
    }

    private static void iterateAndPrint(List<String> strings) {
        strings.forEach(System.out::println);
    }

    private static void swapElements(List<String> strings, int first, int second) {
        String s1 = strings.get(first);
        String s2 = strings.get(second);
        strings.set(second, s1);
        strings.set(first, s2);
        strings.forEach(System.out::println);
    }

    private static List<String> decreaseInitialCapacity(ArrayList<String> strings) {
        strings.trimToSize();
        return strings;
    }

    static int getCapacity(List<String> strings) throws Exception {
        Field field = ArrayList.class.getDeclaredField("elementData");
        field.setAccessible(true);
        return ((Object[]) field.get(strings)).length;
    }
}
