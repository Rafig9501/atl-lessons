import java.util.*;

public class Main {
    public static void main(String[] args) {
        arrayListOperation();
    }

    private static void linkedListOperations(){
        LinkedList<String> strings1 = new LinkedList<>();
        strings1.addFirst("hello");
        strings1.removeFirst();
        strings1.addLast("world");
        strings1.removeLast();
        strings1.removeFirstOccurrence("world");
    }

    private static void arrayListOperation(){
        ArrayList<String> strings = new ArrayList<>();
        List<String> strings2 = new ArrayList<>();
        strings.add("23");
        strings.add("24");
        strings.add("25");
        strings.add("26");
        strings.add("27");
        strings.add("28");
        strings.add("29");
        strings.add("30");
        for (int i = 0; i < strings.size(); i++){
            System.out.print(strings.get(i) + " ");
        }
//        String max = Collections.max(strings);
//        System.out.println(max);
//        int size = strings.size();
//        String s1 = strings.get(0);
//        String s2 = strings.get(1);
//        strings.addAll(strings2);
//        strings.contains("24");
//        strings.indexOf("32");
//        strings.isEmpty();
//        strings.remove("24");
//        strings.removeAll(strings2);
//        strings.subList(0, 1);
//        Object[] objects = strings.toArray();
//
//        System.out.println("element with index 0 is " + s1);
//        System.out.println("element with index 1 is " + s2);
//        System.out.println("Arraylist's size is " + size);
//        System.out.println("Does strings contains element 24 " + strings.contains("24"));
//        System.out.println("Index of 24 is  " + strings.indexOf("24"));
//
//        strings.clear();
//
//        List<AnimalClassChild> animalChildren = new ArrayList<>();
//        List<AnimalClassParent> animalParent = new ArrayList<>();
//
//        animalParent.addAll(animalChildren);
    }

    private static void priorityQueue(){
        PriorityQueue<String> strings = new PriorityQueue<>();
        strings.add("priority1");
        strings.add("priority2");
        strings.add("priority3");
        System.out.println(strings.peek());
        System.out.println(strings.poll());
        System.out.println(strings.peek());
    }
}