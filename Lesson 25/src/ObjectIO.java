import java.io.*;
import java.util.ArrayList;

public class ObjectIO {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("test");
        strings.add("test1");
        strings.add("test2");
        strings.add("test3");
        strings.add("test4");

        File file = new File("Lesson 25/bookings.bin");

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(strings);
        objectOutputStream.close();
        fileOutputStream.close();

        FileInputStream fileInputStream = new FileInputStream(file);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ArrayList<String> readString = (ArrayList<String>) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        readString.forEach(System.out::println);
    }
}
