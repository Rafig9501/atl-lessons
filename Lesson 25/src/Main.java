import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        try {
            File file = new File("Lesson 25/test.txt");
            file.createNewFile();
            BufferedWriter writer = new BufferedWriter(
                new FileWriter("Lesson 25/test.txt"));
            writer.write("7");
            writer.newLine();
            writer.write("8");
            writer.close();
            BufferedReader reader = new BufferedReader(
                new FileReader("Lesson 25/test.txt"));
//            while (reader.readLine() != null) {
//                System.out.println(reader.readLine());
//            }
            reader.close();
            ArrayList<String> strings = new ArrayList<>(
                Files.readAllLines(Path.of("Lesson 25/test.txt")));
            strings.forEach(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}