package exceptions;

import java.io.FileNotFoundException;

public class CustomExceptionExample extends FileNotFoundException {

    public CustomExceptionExample(String s) {
        System.out.println("custom exception is " + s);
    }
}
