package split_strings;

import java.util.Scanner;
import java.util.concurrent.ScheduledExecutorService;

public class StringOps {

    public boolean isCapital(char c) {
        return c >= 'A' && c <= 'Z';
    }

    public boolean isSmall(char c) {
        return c >= 'a' && c <= 'z';
    }

    public boolean isLetter(char c) {
        return isCapital(c) || isSmall(c);
    }

    public boolean isVowel(char c) {
        final String vowels = "aeoiu";
        return vowels.indexOf(Character.toLowerCase(c)) >= 0;
    }

    public boolean isConsonant(char c) {
        return !isVowel(c);
    }

    public String alphabetSmall() {
        StringBuilder alpha = new StringBuilder();
        for (char i = 'a'; i <= 'z'; i++) {
            alpha.append(i);
        }
        return alpha.toString();
    }

    public String alphabetCapital() {
        return alphabetSmall().toUpperCase();
    }

    public char randomSmallLetter() {
        String alpha = alphabetSmall();
        return alpha.charAt((int)(Math.random() * alpha.length()));
    }

    public char randomCapitalLetter() {
        String alpha = alphabetCapital();
        return alpha.charAt((int)(Math.random() * alpha.length()));
    }

    public char randomLetter() {
        return ((int)(Math.random() * 2) == 0) ? // 0 or 1
               randomSmallLetter() : randomCapitalLetter();
    }

    public static void envelopePrint() {
        int width = 22;
        int height = 8;


        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {

                if (i == 0) System.out.print("*");
                if (i == height - 1) System.out.print("*");
                if (j == 0 && i < height - 1 && i != 0) System.out.print("*");
                if (i > 0 && i < height - 1 && j > 0 && j < width - 1) {

                    if ((j == width / 2 || j == width / 2 - 1) && i == height / 2) System.out.print("*");
                    else System.out.print(" ");
                }
                if (i > 0 && i < height - 1 && j == width - 1) System.out.print("*");

            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
//        envelopePrint();
        //        StringOps so = new StringOps();
        //        String str = "ThisIsMyString*743Jow";
        //        //        String[] strArray = str.split("(?=[A-Z])");
        //        //        for (String s : strArray) {
        //        //            System.out.println(s);
        //        //        }
        //        //        String[] strArray2 = str.split("(?=[a-z])");
        //        //        for (String s : strArray) {
        //        //            System.out.println(s);
        //        //        }
        //        //        String[] strArray3 = str.split("(?<=[aeiou'])");
        //        //        for (String s : strArray3) {
        //        //            System.out.println(s);
        //        //        }
        //
        int[][] array = {{1, 2, 3}, {4, 5, 6}, {9, 8, 9}};

//        int sum1 = 0;
//        int sum2 = 0;
//        Scanner scan = new Scanner(System.in);
//
//        int n = scan.nextInt();
//        for (int i = 1; i <= n; i++) {
//            for (int j = 1; j <= n; j++) {
//                int nums = scan.nextInt();
//                if (i == j) {sum1 += nums;}
//                if (n - i + 1 == j) {sum2 += nums;}
//            }
//        }
//        System.out.println(Math.abs(sum1 - sum2));

        int[][] a = {{1, 2, 3}, {4, 5, 6}, {9, 8, 9}};
        int sum1 = 0;
        int sum2 = 0;
        int differ = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (i == j) {
                    sum1 += a[i][j];
                }
                if (i + j == a.length - 1) {
                    sum2 += a[i][j];
                }
            }
        }
        differ = Math.abs(sum1 - sum2);
        System.out.println(differ);
    }

    public static boolean check(String origin, String rotated) {
        return origin.concat(origin).contains(rotated);
    }
}
