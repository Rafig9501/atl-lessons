package equals_hashcode;

public class TernaryExample {

    public static void main(String[] args) {

        int a = 9;
        int b = 3;

        boolean result = a - b == 0 ? false : true;

        if (a - b == 0) {
            result = false;
        }
        else {
            result = true;
        }
    }
}
