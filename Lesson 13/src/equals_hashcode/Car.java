package equals_hashcode;

import java.util.Objects;

public class Car {

    private final String brand;
    private final int year;

    public Car(String brand, int year) {
        this.brand = brand;
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car)o;

        if (year != car.year) return false;
        return Objects.equals(brand, car.brand);
    }

    @Override
    public int hashCode() {
        int result = brand != null ? brand.hashCode() : 0;
        result = 31 * result + year;
        return result;
    }

    @Override
    public String toString() {
        return "Car { brand = " + brand + ", year = " + year + "}";
    }
}
