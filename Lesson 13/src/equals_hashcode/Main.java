package equals_hashcode;

public class Main {

    public static void main(String[] args) {

        Car car1 = new Car("BMW", 1999);
        Car car2 = new Car("BMW", 1999);

        System.out.println(car2.equals(car1));

        System.out.println(car1.hashCode());
        System.out.println(car2.hashCode());

        System.out.println(car1.getClass());

        System.out.println(car1);
    }
}
