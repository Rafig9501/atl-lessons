public class ArrayIntegers {

    public static void main(String[] args) {

        int[] array = new int[2];
        int[] array2 = {2, 3, 4, 5, 6, 7, 8, 9};

        System.out.println(array[0]);
        array[0] = 2;
        System.out.println(array[0]);
        System.out.println(array2[7]);

        for (int i = 0; i < array2.length; i++) {

            System.out.print(array2[i] + " ");
        }
    }
}
