import java.util.Scanner;

public class IsNumberPositiveAndPrime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        if (number > 0) {
            int counter = 0;
            for (int i = 1; i <= number; i++) {
                if (number % i == 0) {
                    counter++;
                }
            }
            if (counter == 2) {
                System.out.println("Number is prime");
            }
        } else {
            System.out.println("Please enter a positive number");
        }
    }
}