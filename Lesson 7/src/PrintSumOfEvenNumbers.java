public class PrintSumOfEvenNumbers {

    //    5. Array verilir, bu arrayin daxilindeki
    //    cut ededlerin cemini ekrana cap edin
    public static void main(String[] args) {

        int[] array = {2, 3, 5, 8, 0, 9};
        int sum = 0;

        int[][] array2 = {{2, 3}, {5, 8}, {0, 9}};
        int sum2 = 0;

        for (int i : array) {
            if (i % 2 == 0) {
                sum = sum + i;
            }
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                sum += array[i];
            }
        }
        System.out.println(sum);

        for (int[] integers : array2) {
            for (int k : integers) {
                if (k % 2 == 0) {
                    sum2 = sum2 + k;
                }
            }
        }
        System.out.println(sum2);
    }
}
