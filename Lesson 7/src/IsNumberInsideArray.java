import java.util.Scanner;

public class IsNumberInsideArray {

    //    Array ve tek eded verilir, ededin bu arrayin daxilinde olub
    //    olmadigini yoxlayan metod yazin
    //    meselen: array={2,45,3,78,90,12,-5,0,33} ,
    //    eded=12 netice: 12 arrayin daxilinde var

    public static void main(String[] args) {

        int[] array = {2, 45, 3, 78, 90, 12, -5, 0, 33};

        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        for (int n : array) {
            if (n == number) {
                System.out.println("Number exists in array");
            }
        }
    }
}
