public class TwoDimensionalArray {

    public static void main(String[] args) {

        int[][] array = new int[3][7];
        int[][][][][][] array4 = new int[3][9][5][1][4][2];

        array4[0][5][3][0][3][1] = 8;

        array[0][0] = 1;
        array[0][6] = 4;
        array[1][3] = 4;

        int[][] array2 = {{2, 3, 4}, {5, 8, 1}, {0, 10, 9}};

        int[] array3 = array2[0];

        int[][] clone = array2.clone();
        int length = array2.length;

        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                System.out.print(array2[i][j] + " ");
            }
        }

        for (int[] ints : array2) {
            for (int i : ints) {
                System.out.print(i + " ");
            }
        }
    }
}
