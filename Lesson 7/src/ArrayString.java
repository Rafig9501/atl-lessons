public class ArrayString {

    public static void main(String[] args) {

        String[] array = new String[8];
        array[0] = "hello";
        array[1] = "world";
        String name = array[0];

        System.out.println(name);

        for (String lastName : array) {
            System.out.println(lastName);
        }

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
