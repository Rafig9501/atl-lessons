package service.impl;

import model.Student;
import service.StudentService;

public class StudentServiceImpl implements StudentService {

    @Override
    public Student calculateGradeInPercentage(Integer grade, Student student) {
        student.setGrade(grade * 100);
        return student;
    }
}
