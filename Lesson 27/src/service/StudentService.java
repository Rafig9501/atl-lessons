package service;

import model.Student;

public interface StudentService {

    Student calculateGradeInPercentage(Integer grade, Student student);
}
