import controller.StudentController;
import model.Student;

public class Main {
    public static void main(String[] args) {

        StudentController studentController = new StudentController();
        studentController.changeStudentGrade(29, new Student("Shakir", 19));

    }
}