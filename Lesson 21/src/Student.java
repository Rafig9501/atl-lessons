public class Student {

    private final Integer grade;

    public Student(Integer grade) {
        this.grade = grade;
    }

    public Integer getGrade() {
        return grade;
    }

    @Override
    public String toString() {
        return "Student{" +
            "grade=" + grade +
            '}';
    }
}
