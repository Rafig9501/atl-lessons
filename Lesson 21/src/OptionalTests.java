import java.util.Optional;

public class OptionalTests {

    public static void main(String[] args) {

        String name = "James";
        Optional<String> stringOptional = Optional.of(name);
        System.out.println(stringOptional);
        if (stringOptional.isPresent()){
            System.out.println(stringOptional.get());
        }
    }
}
