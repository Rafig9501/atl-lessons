import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class Main {
    public static void main(String[] args) {

        new CustomFunctionalInterface<Integer, Integer, Double>() {
            @Override
            public Double doSomething(Integer integer, Integer integer2) {
                return null;
            }
        };

        Integer i = 78;
        Integer j = 78;

        CustomFunctionalInterface<Integer, Integer, Double> f = (i1, i2) -> i1.doubleValue() + i2.doubleValue();

        ArrayList<String> strings = new ArrayList<>();
        strings.add("hello");
        strings.add("world");
        strings.add("foo76y5");
        strings.add("bar432fds");
        strings.add("be543e");

        List<Student> filteredMappedElements = strings.stream()
            .filter(s -> s.startsWith("b") || s.contains("o"))
            .map(s -> s.length())
            .filter(s -> s > 4)
            .sorted((o1, o2) -> o2 - o1)
            .map(s -> new Student(s))
            .collect(Collectors.toList());

        Optional<String> max = strings.stream().max((o1, o2) -> o1.length() - o2.length());

        if (max.isPresent()) {
            System.out.println("max is " + max.get());
        }

        strings.forEach(System.out::println);
        System.out.println("--------------------------------");
        filteredMappedElements.forEach(System.out::println);
    }

    public void test(Optional<String> string) {

        String itIsEmpty = string.orElse("it is empty");

//        String itIsEmpty;
//
//        if (string == null){
//            itIsEmpty = "it is empty";
//        }
//        else {
//            itIsEmpty = string;
//        }

        String string1 = string.orElseThrow(() -> new CustomException("Invalid string"));
    }
}














