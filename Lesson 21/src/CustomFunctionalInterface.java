@FunctionalInterface
public interface CustomFunctionalInterface<T, P, Q> {

    Q doSomething(T t, P p);
}
