public class DifferenceOfEvenAndOdd {

    // 70-e qeder cut ededlerin ceminden tek ededlerin ceminin ferqini hesablayan algorithm yazin

    public static void main(String[] args) {

        int num = 70;
        int sumOfEvens = 0;
        int sumOfOdds = 0;

        for (int i = 1; i < num; i++) {

            if (i % 2 == 0) {
                sumOfEvens += i;
            }

            else {
                sumOfOdds += i;
            }

        }

        System.out.println(sumOfEvens - sumOfOdds);
        System.out.println(sumOfEvens);
        System.out.println(sumOfOdds);
    }
}