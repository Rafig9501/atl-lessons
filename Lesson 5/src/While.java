public class While {

    public static void main(String[] args) {

        int i = 0;

        while (i < 100) {

            System.out.println(i);

            i++;
        }

        for (int j = 0; j < 100; j++) {

            System.out.println(j);

        }
    }
}
