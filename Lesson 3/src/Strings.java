public class Strings {

    public static void main(String[] args) {

        String name = "Adili";
        String lastName = "Adil";

//        System.out.println(name == lastName);
//
//        System.out.println(name.equals(lastName));

        System.out.println(name.length());
        System.out.println(name.toLowerCase());
        System.out.println(name.toUpperCase());
        System.out.println(name.indexOf('i'));
        System.out.println(name.replace('i', 'o'));
        System.out.println(name.compareTo(lastName));

    }
}
