public class SwitchCase {

    public static void main(String[] args) {

        int age = 78;

        switch (age) {
            case 0: {
                System.out.println("age is 0");
                break;
            }
            case 19: {
                System.out.println("age is 19");
                break;
            }
            case 78: {
                System.out.println("age is 78");
                break;
            }
            default:
                System.out.println("age is unknown");
        }
    }
}
