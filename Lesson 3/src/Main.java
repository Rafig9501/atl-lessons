public class Main {

    public static void main(String[] args) {

        int age = 60;

        if (age <= 60) {
            System.out.println("Age is less than 60");
        } else if (age == 60) {
            System.out.println("Age is equal to 60");
        } else {
            System.out.println("Age is greater than 60");
        }
    }
}