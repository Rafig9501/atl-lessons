import java.util.*;

public class HashMapExamplesObjects {

//            hashMap.put(new Animal(9, "rabbit"), "dovshan");

    public static void main(String[] args) {

        HashMap<Animal, String> hashMap = new HashMap<>();

        Animal lion1 = new Animal(12, "lion");
        Animal lion2 = new Animal(12, "lion");

        hashMap.put(lion1, "aslan1");
        hashMap.put(lion2, "aslan2");

        hashMap.entrySet().forEach(System.out::println);
    }
}
