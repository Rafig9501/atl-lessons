import java.util.*;

public class HashSetExamples {

    public static void main(String[] args) {

        HashSet<String> hs = new HashSet<>();
        hs.add("hello");
        hs.add("hello");
        hs.add("hello");
        hs.add("hello");
        hs.add("hello");
        hs.add("hello");
//        hs.forEach(System.out::println);
//        System.out.println("\n");

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("hello");
        arrayList.add("hello");
        arrayList.add("hello");
        arrayList.add("hello");
        arrayList.add("hello");
        arrayList.add("hello");

        arrayList.forEach(System.out::println);
    }
}
