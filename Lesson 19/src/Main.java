import java.util.*;

public class Main {
    public static void main(String[] args) {

        HashMap<Integer, String> map = new HashMap<>();
        map.put(14, "hello");
        map.put(14, "world");
        map.put(15, "goodbye");
        map.put(16, "foo");
        map.put(17, "bar");

        map.put(18, "garden");
        map.put(19, "baby");
        map.put(20, "baby");
        System.out.println(map.containsKey(14));
        System.out.println(map.remove(14));
        System.out.println(map.containsValue("garden"));
        map.entrySet().forEach(System.out::println);
        Set<Integer> keys = map.keySet();
        Collection<String> values = map.values();
        keys.forEach(System.out::println);
        System.out.println("\n");
        values.forEach(System.out::println);
        System.out.println(map.get(14));

        String string = map.getOrDefault(23, "fdsjfbhdsg");

        System.out.println(map.getOrDefault(17, "fdsjfbhdsg"));
        System.out.println(map.get(23));
        map.putIfAbsent(17, "friend");
        System.out.println(map.get(17));
        System.out.println(map.isEmpty());
        HashMap<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put(45, "friend");
        map.putAll(hashMap2);
        map.entrySet().forEach(System.out::println);
        System.out.println(map.size());
        map.clear();
    }
}

// ArrayList<Integer> -> insert complexity -> O(n)
// HashMap<Integer, Integer> -> insert complexity -> O(1) -> O(n)