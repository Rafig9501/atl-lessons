import java.util.Scanner;

public class ScannerTests {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String string = scanner.nextLine();
        byte b = scanner.nextByte();
        int i = scanner.nextInt();
        double d = scanner.nextDouble();
        boolean b1 = scanner.nextBoolean();
        short sh = scanner.nextShort();

        System.out.println("String = " + string);
        System.out.println("byte = " + b);
        System.out.println("int = " + i);
        System.out.println("double = " + d);
        System.out.println("boolean = " + b1);
        System.out.println("short = " + sh);
    }
}
