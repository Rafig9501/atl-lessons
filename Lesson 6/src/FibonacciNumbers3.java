public class FibonacciNumbers3 {

    public static void main(String[] args) {

        int limit = 12;

        int num1;
        int num3;
        int num2 = 1;

        int counter = 0;

        for (num1 = 0; counter < limit; num3 = num1 + num2, num1 = num2, num2 = num3, counter++) {

            System.out.print(num1 + " ");
        }
    }
}
