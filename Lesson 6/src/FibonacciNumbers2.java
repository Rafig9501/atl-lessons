import java.util.Scanner;

public class FibonacciNumbers2 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int limit = scanner.nextInt();

        int num1 = 0;
        int num2 = 1;

        int counter = 0;

        while (counter < limit) {

            System.out.print(num1 + " ");

            int num3 = num1 + num2;

            num1 = num2;

            num2 = num3;

            ++counter;
        }
    }
}