import com.sun.source.tree.Tree;

import java.util.*;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(Collections.reverseOrder());
        Integer val;
        priorityQueue.add(10);
        priorityQueue.add(22);
        priorityQueue.add(36);
        priorityQueue.add(25);
        priorityQueue.add(16);
        priorityQueue.add(70);
        priorityQueue.add(82);
        priorityQueue.add(89);
        priorityQueue.add(14);
        while( (val = priorityQueue.poll()) != null) {
            System.out.print(val+"  " + "\n");
        }
    }

    private static void enumSetOperations() {
        EnumSet<Name> enums = EnumSet.of(Name.George, Name.John, Name.Nick, Name.Edward);
        enums.remove(Name.George);
        enums.forEach(System.out::println);
    }

    private static void hashSetOperations() {
        Set<String> set = new HashSet<>();
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("foo");
        hashSet.add("bar");
        hashSet.add("hello");
        hashSet.add("world");
        int size = hashSet.size();
        boolean hashSetEmpty = hashSet.isEmpty();
        boolean foo = hashSet.remove("foo");
        boolean isDeleted = hashSet.contains("bar");
        Object[] objects = hashSet.toArray();
        hashSet.addAll(set);

        System.out.println("hashSet size is " + size);
        System.out.println("hashSet empty is " + hashSetEmpty);
        System.out.println("bar deleted is " + isDeleted);
        hashSet.forEach(System.out::println);
        hashSet.clear();
    }

    private static void linkedHashSetOperations() {
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.add("foo");
        linkedHashSet.add("bar");
        linkedHashSet.add("hello");
        linkedHashSet.forEach(System.out::println);
    }

    private static void treeSetOperations() {
        TreeSet<String> treeSet = new TreeSet<>();
        treeSet.add("India");
        treeSet.add("Australia");
        treeSet.add("South Africa");
        Iterator<String> ascendingIterator = treeSet.iterator();
        Iterator<String> descendingIterator = treeSet.descendingIterator();
        while (ascendingIterator.hasNext()) {
            System.out.println(ascendingIterator.next());
        }
        while (descendingIterator.hasNext()) {
            System.out.println(descendingIterator.next());
        }
    }

    private static void treeSetComparator() {
        TreeSet<Student> treeSet = new TreeSet<>(new StudentAgeComparator());
        PriorityQueue<Student> priority = new PriorityQueue<>(new StudentAgeComparator());
        treeSet.add(new Student(16));
        treeSet.add(new Student(15));
        treeSet.add(new Student(17));

        treeSet.forEach(System.out::println);
        treeSet.forEach(new Consumer<Student>() {
            @Override
            public void accept(Student x) {
                System.out.println(x);
            }
        });
    }
}
