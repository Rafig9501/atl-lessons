public class Main {

    // Сircle   ->    A = π * r^2, π = 3.14
    // Square   ->    S = a^2
    // Triangle ->    T = a * h/2

    public static void main(String[] args) {
        Circle circle = new Circle(5.0);
        System.out.println("Area of circle is " + circle.calculateArea());

        Triangle triangle = new Triangle(3.1, 8.9);
        System.out.println("Area of triangle is " + triangle.calculateArea());

        Square square = new Square(7.3);
        System.out.println("Area of square is " + square.calculateArea());
    }
}