public class OrderOfInitialization {

    OrderOfInitialization(int x) {System.out.println("Argument constructor");}

    OrderOfInitialization() {System.out.println("No argument constructor");}

    static {System.out.println("1st static init");}

    {System.out.println("3rd instance init");}

    {System.out.println("4th instance init");}

    static {System.out.println("2nd static init");}
}
