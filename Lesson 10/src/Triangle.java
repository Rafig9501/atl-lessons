public class Triangle extends Figure {
    private Double a;
    private Double h;
    public Triangle(Double a, Double h) {
        this.a = a;
        this.h = h;
    }
    @Override
    public Double calculateArea() {
        //        a * (h/2)
        return this.a * (this.h / 2);
    }
}
