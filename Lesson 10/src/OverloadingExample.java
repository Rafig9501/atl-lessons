public class OverloadingExample {

    public static void main(String[] args) {

    }

    static void printGreenAndRed(String green, String red) {
        System.out.println(green + red);
    }

    static int printGreenAndRed(String green) {
        System.out.println(green);
        return 1;
    }
}
