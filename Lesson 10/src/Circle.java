public class Circle extends Figure{
    private Double r;
    public Circle(Double r) {
        this.r = r;
    }
    @Override
    public Double calculateArea() {
//        π * r^2
        double p = 3.14;
        return Math.pow(this.r, 2) * p;
    }
}
