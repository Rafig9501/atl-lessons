public class Square extends Figure {

    public static String name = "Square";
    private Double a;
    public Square(Double a) {
        this.a = a;
    }
    @Override
    public Double calculateArea() {
        //        S = a^2
        return Math.pow(a, 2);
    }
}