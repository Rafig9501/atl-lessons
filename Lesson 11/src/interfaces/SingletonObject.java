package interfaces;

public class SingletonObject {

    private static SingletonObject object;

    private SingletonObject() {}

    public static SingletonObject getSingletonObject() {
        if (object == null) {
            object = new SingletonObject();
            return object;
        }
        return object;
    }
}
