package interfaces;

public final class Animal implements AnimalActions, AnimalOtherActions {

    private final int age = 12;

    @Override
    public void breath() {
        System.out.println("Animal is breathing");
    }

    @Override
    public void run() {
        System.out.println("Animal is running");
    }

    public final void doSomething(){
        System.out.println("doing something");
    }
}
