package interfaces;

public enum AnimalName {

    LION("Shiraslan"),
    RABBIT("Dovshan");

    public final String name;

    AnimalName(String name) {
        this.name = name;
    }
}
