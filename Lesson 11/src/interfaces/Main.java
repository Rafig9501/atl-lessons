package interfaces;

public class Main {

    public static void main(String[] args) {

//        AnimalName animalName = AnimalName.RABBIT;
//        System.out.println(animalName.name);

        SingletonObject object = SingletonObject.getSingletonObject();
        SingletonObject object2 = SingletonObject.getSingletonObject();
        SingletonObject object3 = SingletonObject.getSingletonObject();
        SingletonObject object4 = SingletonObject.getSingletonObject();

        System.out.println(object);
        System.out.println(object2);
        System.out.println(object3);
        System.out.println(object4);
    }
}
