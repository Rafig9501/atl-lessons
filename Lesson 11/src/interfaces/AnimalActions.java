package interfaces;

public interface AnimalActions {

    void breath();

    void run();

    default void sleep(String name) {
        System.out.println(name + " is sleeping");
    }
}
