package interfaces;

public class GarbageCollectionExample {

    public static void main(String[] args) {

        Lion lion = new Lion();
        lion = null;

        System.gc();
    }
}
