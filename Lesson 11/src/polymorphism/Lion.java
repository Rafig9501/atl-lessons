package polymorphism;

public class Lion extends Animal{

    @Override
    public void printName() {
        System.out.println("polymorphism.Lion name: Aslan");
    }

    public void printName(String name) {
        System.out.println("polymorphism.Lion name: "+ name);
    }
}
