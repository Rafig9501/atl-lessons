public class Factorial {

    public static void main(String[] args) {

        long fact = 1;
        long number = 24;

        for (int i = 1; i <= number; i++) {
            fact = fact * i;
        }

        System.out.println("Factorial of " + number + " is: " + fact);
    }
}
