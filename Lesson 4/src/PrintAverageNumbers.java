public class PrintAverageNumbers {

    public static void main(String[] args) {

        double sum = 0;
        int count = 100;

        for (int i = 1; i < count; i++) {
            sum += i;
        }

        System.out.println(sum / count);
        System.out.println(sum);
        System.out.println(count);
    }
}
