public class BreaksInForLoop {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {

            if (i % 2 == 0) {
                System.out.println("i is an odd number");
                break;
            }

            else if (i % 2 == 1) {
                System.out.println("i is an even number");
                continue;
            }

        }
    }
}
