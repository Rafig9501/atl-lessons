import java.util.*;


public class Main {

    public static void main(String[] args) {
        hashSetAdd();
        hashSetConvertToList();
        hashSetConvertToArray();
        hashSetConvertTreeSet();
        hashSetRemoveAllElements();
        treeSet();
        treeSetIterate();
        treeSetGetNumberOfElements();
        compareTwoTreeSet();
        hashSetCompare();
        treeSetRetrieveAndRemoveFirst();
        treeSetRetrieveAndRemoveLast();
    }
    private static void hashSetAdd(){
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("blue");
        hashSet.add("pink");
        hashSet.add("purple");
        hashSet.add("green");
        hashSet.forEach(System.out::println);
        System.out.println("___________");
    }
    private static void hashSetConvertToList() {

        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("blue");
        hashSet.add("pink");
        hashSet.add("purple");
        hashSet.add("green");
        ArrayList<String> list = new ArrayList<>();
        list.addAll(hashSet);
        System.out.println(list);
        System.out.println("___________");
    }
    private static void  hashSetConvertToArray() {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("blue");
        hashSet.add("pink");
        hashSet.add("purple");
        hashSet.add("green");
        String[] array = new String[hashSet.size()];
        hashSet.toArray(array);
        for (String i : array) {
            System.out.println(i);
            System.out.println("___________");
        }
    }
    private static void hashSetConvertTreeSet(){
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("blue");
        hashSet.add("pink");
        hashSet.add("purple");
        hashSet.add("green");
        TreeSet<String> treeSet = new TreeSet<>(hashSet);
        System.out.println(treeSet);
        System.out.println("___________");
    }
    private static void hashSetRemoveAllElements(){
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("blue");
        hashSet.add("pink");
        hashSet.add("purple");
        hashSet.add("green");
        hashSet.clear();
        System.out.println(hashSet);
        System.out.println("___________");
    }
    private static void treeSet(){
        TreeSet<String> treeSet = new TreeSet<>();
        treeSet.add("Blue");
        treeSet.add("Green");
        treeSet.add("Purple");
        treeSet.add("Black");
        System.out.println(treeSet);
        System.out.println("___________");
    }
    private static void treeSetIterate(){
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(6);
        treeSet.add(152);
        treeSet.add(78);
        treeSet.add(3);
        Iterator<Integer> iterator = treeSet.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("___________");

    }
    private static void treeSetGetNumberOfElements() {
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(6);
        treeSet.add(152);
        treeSet.add(78);
        treeSet.add(3);
        int size = treeSet.size();
        System.out.println(size);
        System.out.println("___________");
    }
    private static void compareTwoTreeSet() {
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(6);
        treeSet.add(152);
        treeSet.add(78);
        treeSet.add(3);
        TreeSet<Integer> treeSet1 = new TreeSet<>();
        treeSet1.add(152);
        treeSet1.add(6);
        treeSet1.add(78);
        treeSet1.add(3);
        System.out.println(treeSet.equals(treeSet1));
        System.out.println("___________");


    }
    private static void hashSetCompare(){
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("blue");
        hashSet.add("pink");
        hashSet.add("purple");
        hashSet.add("green");
        HashSet<String> hashSet1 = new HashSet<>();
        hashSet.add("black");
        hashSet.add("red");
        hashSet.add("purple");
        hashSet.add("green");
        System.out.println(hashSet.equals(hashSet1));
        System.out.println("___________");
    }
    private static void treeSetRetrieveAndRemoveFirst(){
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(6);
        treeSet.add(152);
        treeSet.add(78);
        treeSet.add(3);
        System.out.println(treeSet.first());
        treeSet.pollFirst();
        System.out.println(treeSet);
        System.out.println("___________");
    }
    private static void  treeSetRetrieveAndRemoveLast(){
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(6);
        treeSet.add(152);
        treeSet.add(78);
        treeSet.add(3);
        System.out.println(treeSet.last());
        treeSet.pollLast();
        System.out.println(treeSet);
        System.out.println("___________");
    }

}