package org.example;

import org.apache.commons.io.FileUtils;

import javax.swing.text.DateFormatter;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        FileUtils utils = new FileUtils();
        LocalDate localDate = LocalDate.now();
        LocalDate customDate = LocalDate.of(1999, 1, 1);
        LocalDate localDate1 = localDate.minusDays(10);
//        System.out.println(localDate1);
//        System.out.println(localDate.compareTo(customDate));
//        System.out.println(localDate);
//        System.out.println(customDate);

        LocalDateTime localDateTime = LocalDateTime.now();
//        System.out.println(localDateTime);

        String date = "2023-02-07";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate2 = LocalDate.parse(date, dateFormatter);
//        System.out.println(localDate2);

        String dateTime = "2023-02-07 17:29";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime localDateTime1 = LocalDateTime.parse(dateTime, formatter);
        System.out.println(localDateTime1);
    }
}