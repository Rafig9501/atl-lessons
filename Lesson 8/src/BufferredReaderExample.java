import java.io.*;

public class BufferredReaderExample {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int integer = Integer.parseInt(reader.readLine());
        String s = reader.readLine();

//        int i = Integer.parseInt("45");
//        double v = Double.parseDouble("45.21");
//        System.out.println(i);

        System.out.println(integer);
        System.out.println(s);
    }
}
