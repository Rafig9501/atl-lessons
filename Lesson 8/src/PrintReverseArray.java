import java.lang.reflect.Array;
import java.util.*;

public class PrintReverseArray {

    //    13. Array verilir, bu arrayi tersine ceviren kod yazin
    //    meselen: {1,2,3,4,5} in tersi {5,4,3,2,1}

    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4, 5};
        int[] reverseArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            reverseArray[i] = array[array.length - i - 1];
        }

        for (int k : reverseArray) {
            System.out.println(k);
        }

        Collections.reverse(Arrays.asList(Arrays.stream(array)
            .boxed().toArray(Integer[]::new)));
    }
}
