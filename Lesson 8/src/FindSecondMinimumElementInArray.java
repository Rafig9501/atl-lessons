public class FindSecondMinimumElementInArray {

    //     Array verilir, bu arrayin daxilindeki en kicik 2ci ededi tapin
    //     array={2,45,3,78,90,12,-5,0,33}  en kicik ikinci eded=0

    public static void main(String[] args) {

        int[] array = {2, 45, 3, 78, 90, 12, -5, 0, 33};

        int second = Integer.MAX_VALUE;
        int first = Integer.MAX_VALUE;

        for (int j : array) {
            if (j < first) {
                second = first;
                first = j;
            } else if (j < second && j != first) {
                second = j;
            }
        }
        System.out.println(second);
    }
}
