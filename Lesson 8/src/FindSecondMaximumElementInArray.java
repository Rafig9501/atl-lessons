public class FindSecondMaximumElementInArray {

    //    10. Array verilir, bu arrayin daxilindeki en boyuk 2ci ededi tapin
    //    array={2,45,3,78,90,12,-5,0,33}  en boyuk ikinci eded=78

    public static void main(String[] args) {

        int[] array = {2, 45, 3, 90, 12, -5, 0, 78,33};

        int first = Integer.MIN_VALUE;
        int second = Integer.MIN_VALUE;

        for (int j : array) {
            if (j > first) {
                second = first;
                first = j;
            }

            else if (j > second && j != first) {
                second = j;
            }
        }
        System.out.println(second);
    }
}
