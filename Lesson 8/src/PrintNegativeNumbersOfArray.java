public class PrintNegativeNumbersOfArray {

    //    1. Array verilir, bu arrayin daxilindeki menfi ededleri ekrana cap edin.

    public static void main(String[] args) {

        int[] array = {3, -1, 9, 2, -4, -7, -8, 10, 13, -2, 6};

        for (int i : array) {
            if (i < 0) {
                System.out.println(i);
            }
        }
    }
}
