import java.util.Arrays;

public class ArrraysEquality {

    public static void main(String[] args) {

        int[] arr1 = {1, 2, 3, 5, 6};
        int[] arr2 = {1, 2, 3, 5, 6};

        boolean result = true;

        if (arr1.length == arr2.length) {
            for (int i = 0; i < arr1.length; i++) {
                if (arr1[i] != arr2[i]) {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }
        if (result) {
            System.out.println("Arrays are equal");
        } else {
            System.out.println("Arrays are not equal");
        }

        System.out.println(arr1 == arr2);

        System.out.println(Arrays.equals(arr1, arr2));
    }
}
