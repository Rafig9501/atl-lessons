public class FindCommonElements {

    public static void main(String[] args) {

        int[] arr1 = {78, 34, 1, 3, 90, 34, -1, -4, 6, 55, 20, -65};
        int[] arr2 = {78, 3, 17, 32, 90, 31, -3, -5, 9, 87, 2, -65};

        for (int k : arr1) {
            for (int i : arr2) {
                if (k == i) {
                    System.out.print(k);
                }
            }
        }
    }
}
