public class FindMinimumElementInArray {

//    Array verilir, bu arrayin daxilindeki en kicik ededi tapin
    public static void main(String[] args) {

        int[] array = {3, -1, 9, 2, -4, -7, -8, 10, 13, -2, 6};

        int min = array[0];

        for (int i : array) {
            if (i < min) {
                min = i;
            }
        }

        System.out.println(min);
    }
}