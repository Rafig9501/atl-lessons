public class ArraySumOfDigits {

    public static void main(String[] args) {

        int[] arr = {78, 34, -211};

        int sum = 0;
        int digit = 0;

        for (int i : arr) {
            while (i != 0) {
                digit = i % 10;
                sum = sum + digit;
                i = i / 10;
            }
        }
        System.out.println(sum);
    }
}
