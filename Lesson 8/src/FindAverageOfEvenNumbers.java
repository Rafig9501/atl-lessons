public class FindAverageOfEvenNumbers {

    public static void main(String[] args) {
        int[] arr = {78, 34, 1, 3, 90, 34, -1, -4, 6, 55, 20, -65};

        double sum = 0;
        int count = 0;

        for (int i : arr) {
            if (i % 2 == 0) {
                sum += i;
                count++;
            }
        }
        System.out.println(sum / count);
    }
}
